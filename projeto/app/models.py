from django.db import models
from django.contrib.auth.models import User


''''Pergunta'''
PERGUNTA_CHOICES = (
    ("", "Selecione o tipo da pergunta"),
    ("texto", "Aberta"),
    ("escolha", "Múltipla Escolha"),
    
)

class Pergunta(models.Model):
    titulo = models.CharField(max_length=3000)
    tipo = models.CharField(
        max_length=30,
        choices=PERGUNTA_CHOICES,
        default="",
        blank=False
    )

    def __str__(self):
        return self.titulo


class OpcaoPergunta(models.Model):
    letra = models.CharField(max_length=1, blank=False)
    valor = models.CharField(max_length=300)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE, related_name='images')

    def __str__(self):
        return self.valor

class Resposta(models.Model):
    resposta = models.CharField(max_length=3000)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, default="")

    def __str__(self):
        return self.resposta


class Prova(models.Model):
    questoes = models.ForeignKey(Pergunta, on_delete=models.DO_NOTHING)

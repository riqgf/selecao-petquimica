from django.shortcuts import render
from django.views import generic
from datetime import datetime, timedelta, time
from app import models
from django.urls import reverse_lazy, reverse
from django.http import JsonResponse, HttpResponseRedirect, Http404
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin



class IndexView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'pages/index.html'

class SobreView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'pages/sobre.html'
    atual = datetime.now()
    atual = atual.strftime("%d-%m-%y %H-%M")
    print(atual)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        data['atual'] = self.atual

        return data



class PerguntaCreateView(LoginRequiredMixin, generic.CreateView):
    template_name = 'pages/pergunta_form.html'
    model = models.Pergunta
    fields= "__all__"

    def get_success_url(self):
        messages.success(self.request, 'Pergunta criada com sucesso')
        return reverse_lazy('sobre')


    def post(self, request, *args, **kwargs):
        dados = request.POST

        tipo = dados.get('tipo')
        titulo = dados.get('titulo')

        pergunta = models.Pergunta(tipo=tipo, titulo=titulo)
        pergunta.save()
        if tipo == 'escolha':

            letra_a = models.OpcaoPergunta(letra="a", valor=dados.get('letra-a'), pergunta=pergunta)
            letra_a.save()

            letra_b = models.OpcaoPergunta(letra="b", valor=dados.get('letra-b'), pergunta=pergunta)
            letra_b.save()

            letra_c = models.OpcaoPergunta(letra="c", valor=dados.get('letra-c'), pergunta=pergunta)
            letra_c.save()

            letra_d = models.OpcaoPergunta(letra="d", valor=dados.get('letra-d'), pergunta=pergunta)
            letra_d.save()

        return HttpResponseRedirect(self.get_success_url())


class PerguntaListView(LoginRequiredMixin, generic.ListView):
    model = models.Pergunta
    context_object_name = 'perguntas'
    template_name = 'pages/pergunta_list.html'



class ProvaView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'pages/fazer_prova.html'

    def get_context_data(self, **kwargs):
        context = super(ProvaView, self).get_context_data(**kwargs)
        
        context['perguntas'] = models.Pergunta.objects.all()
        context['alternativas'] = models.OpcaoPergunta.objects.all()
        
        return context





def salvarResposta(request):
    # Depois será validado pelo horario da prova
    prova_disponivel = True

    if prova_disponivel:
        dados = request.GET

        user_id = request.user.id
        pergunta_id = dados.get('pergunta_id')
        resposta_recebida = dados.get('resposta')

        existe_resposta = models.Resposta.objects.filter(pergunta_id=pergunta_id, user_id=user_id).exists()

        if not existe_resposta:
            resposta = models.Resposta(user_id=user_id, pergunta_id=pergunta_id)
            resposta.save()
        else:
            resposta = models.Resposta.objects.get(user_id=user_id, pergunta_id=pergunta_id)
            resposta.resposta = resposta_recebida
            resposta.save()

    response = existe_resposta



    return JsonResponse(response, safe=False)


def carregaRespostas(request):

    user_id = request.user.id

    respostas = models.Resposta.objects.filter(user_id=user_id).values()


    return JsonResponse(list(respostas), safe=False)



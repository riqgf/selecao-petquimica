from django.urls import path, include
from app import views


urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('sobre', views.SobreView.as_view(), name="sobre"),
    path('criar_pergunta', views.PerguntaCreateView.as_view(), name="criar_pergunta"),
    path('perguntas', views.PerguntaListView.as_view(), name="perguntas"),



    path('fazer_prova', views.ProvaView.as_view(), name="fazer_prova"),

    path('api/salvar_resposta', views.salvarResposta, name="salvar_resposta"),
    path('api/carrega_respostas', views.carregaRespostas, name="carrega_respostas"),
    path('api/carrega_respostas', views.carregaRespostas, name="carrega_respostas"),






]

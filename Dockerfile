FROM python:3.8-alpine
ENV PYTHONUNBUFFERED=1
WORKDIR /projeto
COPY ./projeto /projeto/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt


EXPOSE 8000